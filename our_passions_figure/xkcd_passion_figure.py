from matplotlib import pyplot as plt
import numpy as np

# --------------------------------------------
# INPUT & CONTROL
# --------------------------------------------
xx = np.linspace(0 + np.finfo(float).eps, 1, 50)
yy_a = 1 - 0.9*xx
# yy_e  = 0.1*1/(1 - xx)  # hyperbolic
# yy_e  = 0.15*1/(1 - xx) - 0.05  # hyperbolic
# yy_e  = 0.1*np.exp(2.5*xx)  # exponential
# yy_e  = -0.5*np.log(-0.90*xx + 1) + 0.1  # logarithmic
yy_e = 0.45*np.arctanh(xx) + 0.1

y_lim = [0, 1.1]

# --------------------------------------------
# PLOT
# --------------------------------------------
plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.set_xticks([0, 1])
ax.xaxis.set_ticks_position('bottom')
ax.set_xticklabels(['Philosophy \n&Science', 'Building \n&Tinkering'])
plt.yticks([])
ax.set_ylim(y_lim)

# lines
plt.plot(xx, yy_a, xx, yy_e)

# annotation
ax.text(xx[-1], yy_a[-1], 'Arpad', color='C0')
ax.text(1.*xx[-1], 0.9*min(y_lim[1], yy_e[-1]), 'Eric', color='C1')

# labels and titles
plt.ylabel('Passion intensity')
plt.title("OUR PASSIONS")

# make room for the labels
plt.tight_layout()
# plt.show()

# --------------------------------------------
# SAVE
# --------------------------------------------

plt.savefig('xkcd_passion_figure.png', dpi=300, transparent=False)
plt.savefig('xkcd_passion_figure.svg', dpi=300)
